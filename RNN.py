import pandas as pd
from collections import deque
import random
import numpy as np
#from sklearn import preprocessing
import time
import tensorflow as tf
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Dropout, LSTM, BatchNormalization
from tensorflow.keras.callbacks import TensorBoard
from tensorflow.keras.callbacks import ModelCheckpoint
#from tensorflow.compat.v1.keras.layers import CuDNNLSTM
import matplotlib.pyplot as plt
import ta
import logging
import os
from math import floor
from math import ceil
import plotly.express as px

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'  # FATAL
logging.getLogger('tensorflow').setLevel(logging.FATAL)

SEQ_LEN = 10  # how long of a preceeding sequence to collect for RNN
FUTURE_PERIOD_PREDICT = 1  # how far into the future are we trying to predict?
EPOCHS = 100  # how many passes through our data
BATCH_SIZE = 100  # how many batches? Try smaller batch if you're getting OOM (out of memory) errors.
NAME = f"{SEQ_LEN}-SEQ-{FUTURE_PERIOD_PREDICT}-PRED-{int(time.time())}"  # a unique name for the model

def multiClassify(col, class_count):
	col_copy = np.array(col)

	minimum = min(col)
	maximum = max(col)
	if(minimum > 0):
		diff = maximum - minimum
	else:
		diff = abs(minimum) + maximum

	clas_vals = diff / class_count
	index = minimum + clas_vals
	classes = []
	while(index < maximum):
		classes.append(index)
		index = index + clas_vals


	for clas in range(len(classes)):
	
		if(clas == 0):
			col_copy[col < classes[clas]] = minimum
		elif(clas == len(classes) -1):
			col_copy[col > classes[clas]] = maximum
		else:
			col_copy[(col >= classes[clas-1]) & (col <= classes[clas])] = classes[clas-1]
				
		

	return col_copy

def fuzzyTehnicalLogic(col,indicator):
	# All this stuff can be hardcoded out of the chart observation
	col_copy = np.array(col,dtype=np.float64)
	if(indicator=='MACD'):
		col_copy[col > 40.0] = 1.
		col_copy[(20.0 >= col) | (col <= 40.0)] = 0.8
		col_copy[(-20.0 > col) | (col < 20.0)] = 0.6
		col_copy[(-20.0 >= col) | (col <= -40.0)] = 0.4
		col_copy[col < -40.0] = 0.2

	if(indicator=='RSI'):
		col_copy[col > 70.0] = 1.0
		col_copy[(60.0 >= col) | (col <= 70.0)] = 0.8
		col_copy[(40.0 > col) | (col < 60.0)] = 0.6
		col_copy[(30.0 >= col) | (col <= 40.0)] = 0.4
		#col_copy[(30.0 >= col) | (col <= 70.0)] = 0.25
		col_copy[col < 30.0] = 0.2

	if(indicator=='MACD9'):
		col_copy[col > 20.0] = 1.0
		col_copy[(5.0 >= col) | (col <= 20.0)] = 0.8
		col_copy[(-15.0 > col) | (col < 5.0)] = 0.6
		col_copy[(-25.0 >= col) | (col <= -15.0)] = 0.4
		#col_copy[(-25.0 >= col) | (col <= 20.0)] = 0.25
		col_copy[col < -25.0] = 0.2

	if(indicator=='AROON'):
		col_copy[col > 90.0] = 1.0
		col_copy[(75.0 >= col) | (col <= 90.0)] = 0.8
		col_copy[(-75.0 > col) | (col < 75.0)] = 0.6
		col_copy[(-95.0 >= col) | (col <= -75.0)] = 0.4
		#col_copy[(-25.0 >= col) | (col <= 20.0)] = 0.25
		col_copy[col < -95.0] = 0.2

	if(indicator=='UO'):
		col_copy[col > 65.0] = 1.0
		col_copy[(55.0 >= col) | (col <= 65.0)] = 0.8
		col_copy[(45.0 > col) | (col < 55.0)] = 0.6
		col_copy[(40.0 >= col) | (col <= 45.0)] = 0.4
		#col_copy[(-25.0 >= col) | (col <= 20.0)] = 0.25
		col_copy[col < 40.0] = 0.2

	return col_copy

def calcMACD(df):
	
	twelveEMA = [0] * 11
	twenysixEMA = [0] * 25
	MACD = [0] * 25
	signal = [0] * 33
	sig_que = deque(maxlen=9)
	twelveEMA.append(np.sum(df['close'][:12]/12))
	twenysixEMA.append(np.sum(df['close'][:26]/26))
	counter = 0
	for i in df['close']:

		if(counter > 11):
			#twelveEMA.append(twelveEMA[counter-1]*(2/13)+i*(1-(2/13)))
			twelveEMA.append((i-twelveEMA[counter-1])*(2/13)+twelveEMA[counter-1])

		if(counter == 25):
			MACD.append(twelveEMA[counter]-twenysixEMA[counter])
			sig_que.append(MACD)

		if(counter > 25):
			#twenysixEMA.append(twenysixEMA[counter-1]*(2/27)+i*(1-(2/27)))
			twenysixEMA.append((i-twenysixEMA[counter-1])*(2/27)+twenysixEMA[counter-1])
			MACD.append(twelveEMA[counter]-twenysixEMA[counter])
			sig_que.append(MACD)

		if(counter==33):
			signal.append(np.sum(sig_que)/9)

		if(counter>33):
			#signal.append(signal[counter-1]*(2/10)+MACD[counter]*(1-(2/10))
			signal.append((MACD[counter]-signal[counter-1])*(2/10)+signal[counter-1])

		counter += 1

	return [twelveEMA,twenysixEMA,MACD,signal]

def normalize(df):
	minn = min(df)
	maxx = max(df)
	return (df-minn)/(maxx-minn)


def classify(current, future):
	if float(future) > float(current):
		return 1
	else:
		return 0

def past_classify(current, past):
	if float(past) > float(current):
		return -1
	else:
		return 0

def preprocess_df(df):
	df = df.drop("future", 1)  # don't need this anymore.
	df = df.drop("time", 1)  # don't need this anymore.
	df = df.drop("Symbol", 1)  # don't need this anymore.
	df = df.drop("Volume BTC", 1)  # don't need this anymore.
	df = df.drop("open", 1)  # don't need this anymore.
	#df = df.drop("close", 1)  # don't need this anymore.
	df = df.drop("high", 1)  # don't need this anymore.
	df = df.drop("low", 1)  # don't need this anymore.
	#df = df.drop("volume(BTC)", 1)
	#df = df.drop("open", 1)  # don't need this anymore.
	#df = df.drop("Weighted_Price", 1)  # don't need this anymore.


	
	for col in df.columns:  # go through all of the columns
		
		if (col != "target") and (col != "MACD") and (col != "signal") and (col != "RSI") \
		and (col != "AUP") and (col != "ADOWN") and (col != "AROON") and (col != "UO") and (col != "indicator"):  
		# normalize all ... except for the target itself!
			df[col] = df[col].pct_change()  # pct change "normalizes" the different currencies (each crypto coin has vastly diff values, we're really more interested in the other coin's movements)
			
			df.dropna(inplace=True) # remove the NaNs created by pct_change

			df[col] = df[col].replace(np.inf,0) 

			minn = min(df[col])
			maxx = max(df[col])

			df[col] = df[col].map(lambda x:(x-minn)/(maxx-minn))


	
	df.dropna(inplace=True)  # cleanup again... jic. Those nasty NaNs love to creep in.


	sequential_data = []  # this is a list that will CONTAIN the sequences
	prev_days = deque(maxlen=SEQ_LEN)  # These will be our actual sequences. 
	#They are made with deque, which keeps the maximum length by popping out older values as new ones come in


	for i in df.values:  # iterate over the values
		prev_days.append([n for n in i[:-1]])  # store all but the target
		if len(prev_days) == SEQ_LEN:  # make sure we have 60 sequences!
			sequential_data.append([np.array(prev_days), i[-1]])  # append those bad boys!

	random.shuffle(sequential_data)  # shuffle for good measure.

	buys = []  # list that will store our buy sequences and targets
	sells = []  # list that will store our sell sequences and targets

	for seq, target in sequential_data:  # iterate over the sequential data
		if target == 0:  # if it's a "not buy"
			sells.append([seq, target])  # append to sells list
		elif target == 1:  # otherwise if the target is a 1...
			buys.append([seq, target])  # it's a buy!

	random.shuffle(buys)  # shuffle the buys
	random.shuffle(sells)  # shuffle the sells!

	lower = min(len(buys), len(sells))  # what's the shorter length?

	buys = buys[:lower]  # make sure both lists are only up to the shortest length.
	sells = sells[:lower]  # make sure both lists are only up to the shortest length.

	#sequential_data = buys+sells  # add them together
	#random.shuffle(sequential_data)  # another shuffle, so the model doesn't get confused with 
	#all 1 class then the other.



	X = []
	y = []

	for seq, target in sequential_data:  # going over our new sequential data
		X.append(seq)  # X is the sequences
		y.append(target)  # y is the targets/labels (buys vs sell/notbuy)

	return np.array(X), y  # return X and y...and make X a numpy array!




main_df = pd.DataFrame() # begin empty
#main_df = pd.read_csv('BTC_USD.csv', names=['time', 'open', 'high', 'low', 'close','volume(BTC)', 'volume', 'Weighted_Price'])
main_df = pd.read_csv('reversed_Binance_BTCUSDT_1h.csv', names=['time','Symbol','open','high','low','close','Volume BTC','volume'])

main_df.fillna(method="ffill", inplace=True)  # if there are gaps in data, use previously known values
main_df.dropna(inplace=True)


MACDvals = calcMACD(main_df)
#main_df['12ema'] = MACDvals[0]
#main_df['26ema'] = MACDvals[1]
main_df['MACD'] = MACDvals[2]
main_df['signal'] = MACDvals[3]
main_df['RSI'] = ta.momentum.rsi(main_df['close'])
#main_df['AUP'] = (ta.trend.aroon_up(main_df['close'],14))
#main_df['ADOWN'] = (ta.trend.aroon_down(main_df['close'],14))
#main_df['AROON'] = main_df['AUP'] - main_df['ADOWN']
main_df['UO'] = ta.momentum.uo(main_df['high'],main_df['low'],main_df['close'],7,14,28)
main_df.dropna(inplace=True)
#main_df['indicator'] = main_df['MACD'] + main_df['signal'] + main_df['RSI'] + main_df['RSI'] + main_df['UO'] #+ main_df['future']


#main_df['indicator'] = normalize(main_df['indicator'])


#plt.plot(main_df.index, normalize(main_df['indicator']), label = "plot 1")
#plt.plot(main_df.index, main_df['indicator'], label = "plot 2") 
#plt.plot(main_df.index, main_df['MACD'], label = "plot 3") 
#plt.plot(main_df.index, main_df['RSI'], label = "plot 3")
#plt.xlabel('X-axis') 
#plt.ylabel('Y-axis') 
#plt.legend() 	
#plt.show() 


#main_df['AUP'] = main_df['AUP'] /100
#main_df['ADOWN'] = main_df['ADOWN'] /100
main_df['MACD'] = normalize(multiClassify(main_df['MACD'],20))
main_df['signal'] = normalize(multiClassify(main_df['signal'],20))
main_df['RSI'] = normalize(multiClassify(main_df['RSI'],20))
#main_df['AROON'] = multiClassify(main_df['AROON'],20)
main_df['UO'] = normalize(multiClassify(main_df['UO'],20))
#main_df['MACD'] = fuzzyTehnicalLogic(main_df['MACD'],'MACD')
#main_df['signal'] = fuzzyTehnicalLogic(main_df['signal'],'MACD9')
#main_df['RSI'] = fuzzyTehnicalLogic(main_df['RSI'],'RSI')
#main_df['AROON'] = fuzzyTehnicalLogic(main_df['AROON'],'AROON')
#main_df['UO'] = fuzzyTehnicalLogic(main_df['UO'],'UO')
#main_df.dropna(inplace=True)
#main_df['future'] = main_df['close'].shift(+FUTURE_PERIOD_PREDICT).fillna(0)
#main_df['future'] = list(map(past_classify, main_df['close'], main_df['future']))
#plt.plot(main_df.index, main_df['indicator'], label = "plot 1") 
#plt.plot(main_df.index, main_df['MACD'], label = "plot 2") 
#plt.plot(main_df.index, main_df['signal'], label = "plot 3")
#plt.plot(main_df.index, main_df['RSI'], label = "plot 4")
#plt.plot(main_df.index, main_df['UO'], label = "plot 5")
plt.xlabel('X-axis') 
plt.ylabel('Y-axis') 
plt.legend() 	
#plt.show() 

#main_df['indicator'] = normalize(multiClassify(main_df['indicator'],30))
#plt.plot(main_df.index, main_df['indicator'], label = "plot 1") 
plt.xlabel('X-axis') 
plt.ylabel('Y-axis') 
plt.legend() 	
#plt.show() 


main_df['future'] = main_df['close'].shift(-FUTURE_PERIOD_PREDICT).fillna(0)

main_df['target'] = list(map(classify, main_df['close'], main_df['future']))



times = sorted(main_df.index.values)  # get the times

last_5pct = sorted(main_df.index.values)[-int(0.05*len(times))]  # get the last 5% of the times

validation_main_df = main_df[(main_df.index >= last_5pct)]  # make the validation data where the index is in the last 5%

main_df = main_df[(main_df.index < last_5pct)]  # now the main_df is all the data up to the last 5%
train_x, train_y = preprocess_df(main_df)
validation_x, validation_y = preprocess_df(validation_main_df)




print(f"train data: {len(train_x)} validation: {len(validation_x)}")
print(f"Dont buys: {train_y.count(0)}, buys: {train_y.count(1)}")
print(f"VALIDATION Dont buys: {validation_y.count(0)}, buys: {validation_y.count(1)}")

print(main_df)

train_y = np.asarray(train_y)
validation_y = np.asarray(validation_y)
train_x = np.asarray(train_x)
validation_x = np.asarray(validation_x)

print(train_x)

model = Sequential()
#128 -----------V
model.add(LSTM(128, input_shape=(train_x.shape[1:]), return_sequences=True))
model.add(Dropout(0.2))
model.add(BatchNormalization())

model.add(LSTM(256, return_sequences=True))
model.add(Dropout(0.1))
model.add(BatchNormalization())

model.add(LSTM(128))
model.add(Dropout(0.2))
model.add(BatchNormalization())

model.add(Dense(32, activation='relu'))
model.add(Dropout(0.2))

model.add(Dense(2, activation='softmax'))

opt = tf.keras.optimizers.RMSprop(
    learning_rate=0.001, rho=0.9, momentum=0.0, epsilon=1e-07
)

#opt = tf.keras.optimizers.Adam(lr=0.001, decay=1e-6)

# Compile model
model.compile(
    loss='sparse_categorical_crossentropy',
    optimizer=opt,
    metrics=['accuracy']
)

tensorboard = TensorBoard(log_dir="logs/{}".format(NAME))

filepath = "RNN_Final-{epoch:02d}-{val_accuracy:.3f}"  # unique file name that will include the epoch and the validation acc for that epoch
checkpoint = ModelCheckpoint("models/{}.model".format(filepath, monitor='val_accuracy', verbose=1, save_best_only=True, mode='max')) # saves only the best ones

# Train model
history = model.fit(
    train_x, train_y,
    batch_size=BATCH_SIZE,
    epochs=EPOCHS,
    validation_data=(validation_x, validation_y),
    #callbacks=[tensorboard, checkpoint],
)

# Score model
score = model.evaluate(validation_x, validation_y, verbose=0)
print('Test loss:', score[0])
print('Test accuracy:', score[1])
# Save model
model.save("models/{}".format(NAME))