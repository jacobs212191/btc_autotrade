import pandas as pd
import ta
import matplotlib.pyplot as plt


df = pd.DataFrame() # begin empty
df = pd.read_csv('Binance_BTCUSDT_1h.csv', names=['time','Symbol','open','high','low','close','Volume BTC','volume'], sep=',')

df = df.drop("time", 1)  # don't need this anymore.
df = df.drop("Symbol", 1)  # don't need this anymore.
df = df.drop("Volume BTC", 1)  # don't need this anymore


#df = ta.utils.dropna(df)

df =  df.iloc[::-1]


#df = ta.trend.macd(df['close'])
#df = ta.momentum.rsi(df['close'])
#aup = ta.trend.aroon_up(df['close'],14)
#adown = ta.trend.aroon_down(df['close'],14)
#aroon = aup-adown
uo = ta.momentum.uo = ta.momentum.uo(df['high'],df['low'],df['close'],7,14,28)
plt.plot(uo.index,uo, label='linear')
plt.show()


#print(aup)
#print(adown)
print(uo)
#print(len(df.columns))