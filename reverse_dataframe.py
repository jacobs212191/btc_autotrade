import pandas as pd
import sys

df = pd.DataFrame() # begin empty
df = pd.read_csv(sys.argv[1])
df = df.iloc[::-1]
print(df)

df.to_csv (r'reversed_'+str(sys.argv[1]), index = False, header=False)