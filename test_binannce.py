from binance.client import Client
import tensorflow as tf
import pandas as pd
from collections import deque
import numpy as np
import ta
from math import floor
from math import ceil
import sys


MODEL = str(sys.argv[1])
SEQ_LEN = int(MODEL.split('-')[0].split('/')[1])
client = Client('privateKey', 'publicKey')



def normalize(df):
	minn = min(df)
	maxx = max(df)
	return (df-minn)/(maxx-minn)

def calcMACD(df):
	col = np.array(df['Close'],dtype=np.float64)
	

	twelveEMA = [0] * 11
	twenysixEMA = [0] * 25
	MACD = [0] * 25
	signal = [0] * 33
	sig_que = deque(maxlen=9)
	twelveEMA.append(np.sum(col[:12]/12))
	twenysixEMA.append(np.sum(col[:26]/26))
	counter = 0
	for i in col:

		if(counter > 11):
			#twelveEMA.append(twelveEMA[counter-1]*(2/13)+i*(1-(2/13)))
			twelveEMA.append((i-twelveEMA[counter-1])*(2/13)+twelveEMA[counter-1])

		if(counter == 25):
			MACD.append(twelveEMA[counter]-twenysixEMA[counter])
			sig_que.append(MACD)

		if(counter > 25):
			#twenysixEMA.append(twenysixEMA[counter-1]*(2/27)+i*(1-(2/27)))
			twenysixEMA.append((i-twenysixEMA[counter-1])*(2/27)+twenysixEMA[counter-1])
			MACD.append(twelveEMA[counter]-twenysixEMA[counter])
			sig_que.append(MACD)

		if(counter==33):
			signal.append(np.sum(sig_que)/9)

		if(counter>33):
			#signal.append(signal[counter-1]*(2/10)+MACD[counter]*(1-(2/10))
			signal.append((MACD[counter]-signal[counter-1])*(2/10)+signal[counter-1])

		counter += 1

	return [twelveEMA,twenysixEMA,MACD,signal]

def multiClassify(col, class_count):
	col_copy = np.array(col)

	minimum = min(col)
	maximum = max(col)
	if(minimum > 0):
		diff = maximum - minimum
	else:
		diff = abs(minimum) + maximum

	clas_vals = diff / class_count
	index = minimum + clas_vals
	classes = []
	while(index < maximum):
		classes.append(index)
		index = index + clas_vals


	for clas in range(len(classes)):
	
		if(clas == 0):
			col_copy[col < classes[clas]] = minimum
		elif(clas == len(classes) -1):
			col_copy[col > classes[clas]] = maximum
		else:
			col_copy[(col >= classes[clas-1]) & (col <= classes[clas])] = classes[clas-1]
				
		

	return col_copy

# fetch weekly klines since it listed
candles = client.get_klines(symbol='BTCUSDT', interval=Client.KLINE_INTERVAL_1HOUR)

candles = np.array(candles,dtype=np.float64)

colums = ['OTime','Open','High','Low','Close','Volume','CTime','QAV','NumTrades','TakerBuyAsVol','TakerQuoteAsVol','Ignored']
df = pd.DataFrame (candles,columns=colums)


MACDvals = calcMACD(df)
#df['12ema'] = MACDvals[0]
#df['26ema'] = MACDvals[1]
df['MACD'] = MACDvals[2]
df['signal'] = MACDvals[3]
df['RSI'] = ta.momentum.rsi(df['Close'])
df['UO'] = ta.momentum.uo(df['High'],df['Low'],df['Close'],7,14,28)
df.dropna(inplace=True)
#df['indicator'] = df['MACD'] + df['signal'] + df['RSI'] + df['RSI'] + df['UO']

df['MACD'] = normalize(multiClassify(df['MACD'],20))
df['signal'] = normalize(multiClassify(df['signal'],20))
df['RSI'] = normalize(multiClassify(df['RSI'],20))
df['UO'] = normalize(multiClassify(df['UO'],20))

#print (normalize(df['Volume'].pct_change()))
# pct change "normalizes" the different currencies		
df['Volume'] = df['Volume'].pct_change()
df.dropna(inplace=True) # remove the NaNs created by pct_change
minn,maxx = min(df['Volume']),  max(df['Volume'])
df['Volume'].map(lambda x:(x-minn)/(maxx-minn)) 
'''
df['12ema'] = df['12ema'].pct_change()
df.dropna(inplace=True) # remove the NaNs created by pct_change
minn,maxx = min(df['12ema']),  max(df['12ema'])  
df['12ema'].map(lambda x:(x-minn)/(maxx-minn))

df['26ema'] = df['26ema'].pct_change()
df.dropna(inplace=True) # remove the NaNs created by pct_change
minn,maxx = min(df['26ema']),  max(df['26ema']) 
df['26ema'].map(lambda x:(x-minn)/(maxx-minn)) 
'''

#minn,maxx = min(df['indicator']),  max(df['indicator']) 
#df['indicator'].map(lambda x:(x-minn)/(maxx-minn)) 

df = df.drop("OTime", 1)  # don't need this anymore.
df = df.drop("Open", 1)  # don't need this anymore.
df = df.drop("High", 1)  # don't need this anymore.
df = df.drop("Low", 1)  # don't need this anymore.
df = df.drop("Close", 1)  # don't need this anymore.
df = df.drop("CTime", 1)  # don't need this anymore.
df = df.drop("QAV", 1)  # don't need this anymore.
df = df.drop("NumTrades", 1)  # don't need this anymore.
df = df.drop("TakerBuyAsVol", 1)  # don't need this anymore.
df = df.drop("TakerQuoteAsVol", 1)  # don't need this anymore.
df = df.drop("Ignored", 1)  # don't need this anymore.


sequential_data = []  # this is a list that will CONTAIN the sequences
prev_days = deque(maxlen=SEQ_LEN)  # These will be our actual sequences. 
	#They are made with deque, which keeps the maximum length by popping out older values as new ones come in


for i in df.values:  # iterate over the values
	prev_days.append([n for n in i])
	if len(prev_days) == SEQ_LEN:  
		print(prev_days)
		sequential_data.append(np.array(prev_days))

sequential_data = np.array(sequential_data)

model = tf.keras.models.load_model(MODEL)
predictions = []
predictions = model.predict(sequential_data)

df = df[-len(predictions):]
predtic = []
for pred in predictions:
	if(pred[0] > pred[1]):
		predtic.append(0)
	else:
		predtic.append(1)

df['predictions'] = np.array(predtic)
print(df)