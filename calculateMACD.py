import numpy as np
import pandas as pd
from collections import deque

main_df = pd.DataFrame() # begin empty
#main_df = pd.read_csv('BTC_USD.csv', names=['time', 'open', 'high', 'low', 'close','volume(BTC)', 'volume', 'Weighted_Price'])
main_df = pd.read_csv('Binance_BTCUSDT_1h.csv', names=['time','Symbol','open','high','low','close','Volume BTC','volume'])

main_df =  main_df.iloc[::-1]

def calcMACD(df):
	df = df.drop("time", 1)  # don't need this anymore.
	df = df.drop("Symbol", 1)  # don't need this anymore.
	df = df.drop("Volume BTC", 1)  # don't need this anymore.

	print(np.sum(df['close'][:12]/12))
	
	twelveEMA = [0] * 11
	twenysixEMA = [0] * 25
	MACD = [0] * 25
	signal = [0] * 33
	sig_que = deque(maxlen=9)
	twelveEMA.append(np.sum(df['close'][:12]/12))
	twenysixEMA.append(np.sum(df['close'][:26]/26))
	counter = 0
	for i in df['close']:

		if(counter > 11):
			#twelveEMA.append(twelveEMA[counter-1]*(2/13)+i*(1-(2/13)))
			twelveEMA.append((i-twelveEMA[counter-1])*(2/13)+twelveEMA[counter-1])

		if(counter == 25):
			MACD.append(twelveEMA[counter]-twenysixEMA[counter])
			sig_que.append(MACD)

		if(counter > 25):
			#twenysixEMA.append(twenysixEMA[counter-1]*(2/27)+i*(1-(2/27)))
			twenysixEMA.append((i-twenysixEMA[counter-1])*(2/27)+twenysixEMA[counter-1])
			MACD.append(twelveEMA[counter]-twenysixEMA[counter])
			sig_que.append(MACD)

		if(counter==33):
			signal.append(np.sum(sig_que)/9)

		if(counter>33):
			#signal.append(signal[counter-1]*(2/10)+MACD[counter]*(1-(2/10)))
			signal.append((MACD[counter]-signal[counter-1])*(2/10)+signal[counter-1])

		counter += 1

	return [twelveEMA,twenysixEMA,MACD,signal]

vals = calcMACD(main_df)

for val in vals[3]:
	print(val)