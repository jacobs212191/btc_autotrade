from binance.client import Client
import pandas as pd
import numpy as np
import time
from collections import deque

#client = Client('privatePasswd', 'publicPasswd')

df = pd.DataFrame() # begin empty
df = pd.read_csv('reversed_Binance_BTCUSDT_1h.csv', names=['time','Symbol','open','high','low','close','Volume BTC', \
	'volume'])
df.fillna(method="ffill", inplace=True)  # if there are gaps in data, use previously known values
df.dropna(inplace=True)

df = df.drop("time", 1)  # don't need this anymore.
df = df.drop("Symbol", 1)  # don't need this anymore.
df = df.drop("Volume BTC", 1)  # don't need this anymore.
df = df.drop("open", 1)  # don't need this anymore.
#df = df.drop("close", 1)  # don't need this anymore.
df = df.drop("high", 1)  # don't need this anymore.
df = df.drop("low", 1)  # don't need this anymore.
df = df.drop("volume", 1)  # don't need this anymore.

SEQ_LEN=72

def calcFibLevels(minn, maxx):
	fibs = [0] * 7
	fibs[6] = maxx
	fibs[5] = maxx * 0.786
	fibs[4] = maxx * 0.618
	fibs[3] = maxx * 0.5
	fibs[2] = maxx * 0.382
	fibs[1] = maxx * 0.236
	fibs[0] = minn

	return fibs

balance = 100




#Trading

#candles = client.get_klines(symbol='BTCUSDT', interval=Client.KLINE_INTERVAL_1HOUR)

#candles = np.array(candles,dtype=np.float64)

#colums = ['OTime','Open','High','Low','Close','Volume','CTime','QAV','NumTrades','TakerBuyAsVol','TakerQuoteAsVol','Ignored']
#df = pd.DataFrame (candles,columns=colums)

#df = df[-72:]

sequential_data = []  # this is a list that will CONTAIN the sequences
prev_days = deque(maxlen=SEQ_LEN)  # These will be our actual sequences. 
#They are made with deque, which keeps the maximum length by popping out older values as new ones come in


for i in df.values:  # iterate over the values
	prev_days.append([n for n in i])  # store all but the target
	if len(prev_days) == SEQ_LEN:  # make sure we have 60 sequences!
		sequential_data.append(np.array(prev_days))  # append those bad boys!


counter = 0
for seq in sequential_data:
	#current_price = seq[0]
	if(counter // 72 == 0):
		bought_p = 0
		prev_price = 0
		bought = 0
		levels = calcFibLevels(min(seq),max(seq))
		for current_price in seq:
			print(current_price)
			min_diff = abs(levels[0] - current_price)
			lvl = levels[0]
			for level in levels:
				diff = abs(level-current_price)
				if(diff < min_diff):
					min_diff = diff
					lvl = level

			if(prev_price != 0):

				if(prev_price<=lvl and current_price>=lvl):
					
					if(bought == 1):
						print('sold at: '+str(current_price))
						delta = current_price - bought_p
						delta = (delta * 100) / current_price
						print(delta)
						balance = balance + (balance * delta)
						bought = 0

				elif(prev_price>=lvl and current_price<=lvl):
					if(bought == 0):
						print('bought at: '+str(current_price))
						bought_p = current_price
						
						bought = 1

				else:
					pass

				prev_price = current_price

			else:
				prev_price = current_price
				
		
	counter += 1

